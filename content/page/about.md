---
title: About me
comments: false
---

Hi, I'm Mike. I'm a hybrid cheminformatician / data scientist / software
engineer, and like to build web apps as a hobby. For work I create
machine learning models to make predictions about chemistry. Keep reading
to learn a little bit about me.

{{% center %}}

### [Education](#education) | [Work Experience](#work-experience) | [Projects](#projects)

{{% /center %}}

---

# Education

### University of Florida

#### Ph.D. Computational Chemistry

##### _Aug 2015 -- May 2018_

**Dissertation**: Developing Software to Assess the Viability of Molecular
Mechanics Force Fields for the Prediction of Polymer Properties.
Software release: [pysimm](https://pysimm.org)

### Pennsylvania State University

#### M.S. Computational Materials Science

##### _Aug 2012 -- May 2015_

**Thesis**:
Immunoglobulin G: Solution Dynamics, Carbohydrate Structure,
and Self-association From Atomistic and Coarse-grained Simulations.

### Pennsylvania State University

#### B.S. Materials Science and Enginerring

##### _Aug 2008 -- May 2012_

---

# Work Experience

### Data Scientist

#### Novartist, Cambridge MA

##### _Nov 2020 -- present_

As part of the computer aided drug design group I employ data science and
machine learning techniques to makes predictions about chemistry - anything
from property prediction to synthesis planning.

### Postdoctoral Researcher

#### MIT, Cambridge MA

##### _Oct 2018 -- Oct 2020_

I built and trained neural networks using Python tools like
tensorflow and keras to predict candidate precursors for target
organic molecules. Specifically, my focus was on improving ML predictions
for reaction types in low data regimes. These predictions can be chained together to
create full retrosynthetic trees that end in cheap, buyable starting
materials. This was part of an on-going project as part of the
<a href="https://mlpds.mit.edu" target="_blank">MLPDS</a> at MIT.

### Data Science Contractor

#### Whisker Labs, Maryland

##### _Aug 2018 -- Sep 2018_

I consulted for Whisker Labs yielding insights from real-time sensor
data using supervised and unsupervised machine learning techniques.
This short consulting stint resulted in an interactive dashboard
deliverable enabling data exploration for C-level executives.

### The Data Incubator Fellow

#### The Data Incubator, Washington DC

##### _Jun 2018 -- Aug 2018_

[The Data Incubator](https://www.thedataincubator.com/) is a data science
bootcamp fellowship where I learned a variety of machine learning fundamentals
and applications. If you're interested in an extremely efficient modern
educational environment and machine learning it's absoultely worth applying
for the fellowship. My final project during the fellowship was a
data-driven nba wager prediction engine called
[Bet Smart](http://bet-smart.herokuapp.com/).

### HPCMP Intern

#### Army Research Lab, Aberdeen MD

##### _Jun 2017 -- Aug 2017_

I participated in the High Performance Computing Modernization Program at the
Army Research Lab at Aberdeen Proving Grounds. I worked on understanding
microstructure heterogeneity in energetic materials using molecular simulation
techniques and how it can impact sensitivity and performance. My research
involved post processing of billion-particle molecular systems and resulted
in a python tool leveraging massively parallelized computatioons to
efficiently analyze and filter such large systems into tractable,
visualizable data sets.
[[poster]](https://drive.google.com/open?id=1YvtVxhVcJDDwsIhhVKIOix0czhs3jbK5)

---

# Projects

### ASKCOS

#### [https://askcos.mit.edu](https://askcos.mit.edu) | [https://github.com/ASKCOS/ASKCOS](https://github.com/ASKCOS/ASKCOS)

During my time as a post-doc at MIT, I contributed to the ASKCOS synthesis
planning project from both the software engineering and the scientific sides
of things. I helped convert the academic, monolithic application into a
microservice architecture to help deploy the application in industrial
settings. I also helped build a set of API endpoints for machine learning
predictions, and built a Vue frontend to consume those APIs. The frontend
included a new Interactive Path Planner for chemist-guided retrosynthetic
planning. From the scientific side of things, I developed a data-augmentation
and pre-training workflow to help template-based retrosynthetic neural
networks better learn the context for rare template classes.

### Bet Smart

#### [http://bet-smart.herokuapp.com](http://bet-smart.herokuapp.com)

Bet Smart was the deliverable from my participation in The Data Incubator.
It attempts to predict nba wager outcomes, such as which team will win,
which team will cover the spread, and if the two teams will hit the over.
It involved curating game logs from over 30 years of nba seasons, featurizing
statistics into categorized descriptors, and training various machine learning
models to predict the outcomes. While it doesn't achieve any ground-breaking
performance, it serves as a useful tool to help inform decisions based on
data-driven results!

### Python Simulation Interface for Molecular Modeing

#### [https://pysimm.org](https://pysimm.org) | [https://github.com/polysimtools/pysimm](https://github.com/polysimtools/pysimm)

[pysimm](https://pysimm.org) is a set of python tools for use with molecular simulations.
From structure creation to force field assignment to execution using
[LAMMPS](https://www.lammps.org/) to post processing, pysimm is designed
to help you never leave a python environment. Abstract away cryptic syntax
to assemble simulation workflows with building blocks, for example,
to perform iterative molecular dynamics-monte carlo simulations.

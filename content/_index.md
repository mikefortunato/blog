Somehow you've stumbled upon my blog -- welcome. This started as a project to exercise some CI/CD muscles
but hopefully I'll provide some content worth reading. I'm a computational chemist aspiring to be a
software engineer, so you can expect to blog posts about science and technology, but I'll also throw in
some sports content too.

Hope you enjoy your stay.
